import pandas as pd
import random
import os
import markdown
import argparse
from pathlib import Path
import hashlib


def load_data(students_file, tasks_file):
    students_df = pd.read_csv(students_file) if students_file.endswith('.csv') else pd.read_excel(students_file)
    tasks_df = pd.read_excel(tasks_file, sheet_name=None)
    return students_df, tasks_df['Экзамен (задачи)'], tasks_df['Экзамен (вопросы)']


def generate_problem_sets(coding_tasks, theory_questions, total_students, num_coding=2, num_theory=1):
    total_combinations = len(coding_tasks) * len(theory_questions)

    if total_combinations < total_students:
        raise ValueError("Not enough unique task combinations for the number of students.")

    unique_combinations = set()
    while len(unique_combinations) < total_students:
        coding_indices = sorted(random.sample(range(len(coding_tasks)), num_coding))
        theory_indices = sorted(random.sample(range(len(theory_questions)), num_theory))
        combination = (tuple(coding_indices), tuple(theory_indices))
        unique_combinations.add(combination)

    return [(coding_tasks.iloc[list(coding_indices)], theory_questions.iloc[list(theory_indices)]) for
            coding_indices, theory_indices in unique_combinations]


def generate_md_content(student, tasks, shrek_img):
    md_content = f"# Финальный экзамен - {student.Имя}\n\n"
    md_content += "Это открытый экзамен, вы можете использовать любые материалы, но консультации и обсуждения с другими людьми запрещены.\n\n"
    md_content += "Весь код должен быть написан в чистом функциональном стиле. Никаких сайд-эффектов, изменяемого состояния и прочего, кроме операций с стандартным вводом/выводом.\n\n"
    md_content += "Каждая задача весит 35 баллов и вопрос весит 30 баллов. Максимальное количество баллов за экзамен - 100 баллов (25% от итоговой оценки).\n\n"
    md_content += "У вас есть 1 час 30 минут на выполнение заданий.\n\n"
    md_content += "Удачи!\n\n"
    md_content += "Картинка Шрека для поднятия настроения:\n\n"
    md_content += f"![Alt text](../assets/shreks/{shrek_img})\n\n"

    j = 0
    for task in tasks:
        for row in task.items():
            task_type = "Задача" if j < 2 else "Вопрос"
            task_points = "35" if j < 2 else "30"
            md_content += f"## {task_type} №{j + 1} ({task_points} баллов)\n\n"
            md_content += row[1] + '\n\n'
            j += 1

    return md_content

def read_jpg_filenames(folder_path):
    jpg_files = []
    for file in os.listdir(folder_path):
        if file.endswith(".jpg") or file.endswith(".png") or file.endswith(".jpeg"):
            jpg_files.append(file)

    return jpg_files

def write_output_files(problem_sets, output_dir, students_df, output_format, shreks):
    # Create the output directory if it doesn't exist
    Path(output_dir).mkdir(parents=True, exist_ok=True)

    file_names = []
    for i, (student, tasks) in enumerate(zip(students_df.itertuples(index=False), problem_sets)):
        base_file_name = hashlib.md5(student.gitlab_id.strip().encode()).hexdigest()
        md_content = generate_md_content(student, tasks, shreks[i % len(shreks)])
        file_names.append(base_file_name)

        if output_format in ['md', 'both']:
            md_file_name = f"{base_file_name}.md"
            with open(os.path.join(output_dir, md_file_name), 'w', encoding='utf-8') as file:
                file.write(md_content)

        if output_format in ['html', 'both']:
            html_file_name = f"{base_file_name}.html"
            html_content = markdown.markdown(md_content, extensions=["fenced_code"])
            with open(os.path.join(output_dir, html_file_name), 'w', encoding='utf-8') as file:
                file.write(
                    f"""
                    <html>
                        <head>
                            <style>
                                .markdown-body {{
                                    box-sizing: border-box;
                                    min-width: 200px;
                                    max-width: 980px;
                                    margin: 0 auto;
                                    padding: 45px;
                                }}
                            
                                @media (max-width: 767px) {{
                                    .markdown-body {{
                                        padding: 15px;
                                    }}
                                }}
                            </style>
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"../assets/main.css\">
                            <meta charset=\"UTF-8\">
                        </head>
                        <body class=\"markdown-body\">{html_content}</body></html>"""
                )

    return file_names


def create_mapping_table(students_df, problem_sets, file_names):
    task_mappings = []

    for student, (coding_tasks, theory_tasks), file_name in zip(students_df.itertuples(index=False), problem_sets,
                                                                file_names):
        coding_indices = [f"Coding {i + 1}" for i in coding_tasks.index]
        theory_indices = [f"Theory {i + 1}" for i in theory_tasks.index]
        task_indices = ', '.join(coding_indices + theory_indices)
        task_mappings.append({
            'Name': student.Имя,
            'Email': student.Email,
            'AssignedFile': file_name,
            'TaskIndices': task_indices
        })

    return pd.DataFrame(task_mappings)


def main():
    parser = argparse.ArgumentParser(description='Generate unique problem sets for students.')
    parser.add_argument('--format', type=str, choices=['md', 'html', 'both'], default='both',
                        help='Output format: md, html, or both')
    parser.add_argument('students_file', type=str, help='Path to the file containing student names and emails')
    parser.add_argument('tasks_file', type=str, help='Path to the file containing tasks')
    parser.add_argument('--output_dir', type=str, default='output',
                        help='Directory to save the generated markdown files')
    parser.add_argument('--seed', type=int, default=42, help='Random seed for reproducibility')
    args = parser.parse_args()

    random.seed(args.seed)

    try:
        students_df, coding_tasks, theory_questions = load_data(args.students_file, args.tasks_file)
        problem_sets = generate_problem_sets(coding_tasks['Перевод Текста Задачи'],
                                             theory_questions['Перевод'], len(students_df))
        output_dir = Path(args.output_dir)
        output_dir.mkdir(parents=True, exist_ok=True)
        shreks = read_jpg_filenames("assets/shreks")
        file_names = write_output_files(problem_sets, output_dir, students_df, args.format, shreks)
        mapping_table = create_mapping_table(students_df, problem_sets, file_names)
        mapping_table.to_csv(output_dir / 'student_task_mapping.csv', index=False)
    except ValueError as e:
        print(f"Error: {e}")


if __name__ == "__main__":
    main()
